package com.example.linger;

import android.content.Intent;
import android.os.Bundle;

import com.example.linger.Fragments.DefaultFragment;
import com.example.linger.Fragments.GraffitoMsgFragment;
import com.example.linger.Fragments.MemoMsgFragment;
import com.example.linger.Fragments.MsgFragment;
import com.example.linger.Fragments.NoteMsgFragment;
import com.example.linger.Fragments.SettingFragment;

import it.neokree.materialnavigationdrawer.MaterialNavigationDrawer;
import it.neokree.materialnavigationdrawer.elements.MaterialAccount;


public class MsgActivity extends MaterialNavigationDrawer {

    MaterialAccount mAccount;
    GraffitoMsgFragment graffitoMsgFragment;
    MemoMsgFragment memoMsgFragment;
    NoteMsgFragment noteMsgFragment;
    SettingFragment settingFragment;
    @Override
    public void init(Bundle bundle) {

        //初始化账户
        initAccount();

        graffitoMsgFragment = new GraffitoMsgFragment();
        memoMsgFragment = new MemoMsgFragment();
        noteMsgFragment = new NoteMsgFragment();
        settingFragment = new SettingFragment();
        //Add sections
        this.addSection(newSection("Graffito",R.drawable.ic_gesture_black_36dp, graffitoMsgFragment));
        this.addSection(newSection("Memo", R.drawable.ic_create_black_36dp, memoMsgFragment));
        this.addSection(newSection("Notes", R.drawable.ic_content_paste_black_36dp,noteMsgFragment));

        //Settings
        this.addBottomSection(newSection("Settings", R.drawable.settings_ic, settingFragment));
    }

    private void initAccount(){
        //todo:根据之前传来的信息初始化账户信息
        mAccount = new MaterialAccount(this.getResources(), "IvanWang", "ivan.wang.sh@gmail.com",
                R.drawable.default_photo, R.drawable.default_account_bkg);
        this.addAccount(mAccount);
    }
}

