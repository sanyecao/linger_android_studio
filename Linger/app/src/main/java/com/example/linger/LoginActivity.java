package com.example.linger;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.linger.util.ServiceUtil;
import com.gc.materialdesign.views.ButtonFlat;
import com.rengwuxian.materialedittext.MaterialEditText;


public class LoginActivity extends Activity {

    private static final int PASSWORD_NOT_CONFIRM = 0;
    private static final int PASSWORD_CONFIRMED = 1;

    private ButtonFlat regBtn;
    private ButtonFlat logBtn;
    private ButtonFlat tryBtn;
    private MaterialEditText userIDEt;
    private MaterialEditText passwordEt;
    private MaterialEditText confirmEt;
    private LinearLayout loginrootLl;

    private int passwordState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        passwordState = PASSWORD_NOT_CONFIRM;

        regBtn = (ButtonFlat)findViewById(R.id.button_reg);
        logBtn = (ButtonFlat)findViewById(R.id.button_log);
        tryBtn = (ButtonFlat)findViewById(R.id.button_try);
        userIDEt = (MaterialEditText)findViewById(R.id.editText_userID);
        passwordEt = (MaterialEditText)findViewById(R.id.editText_password);
        confirmEt = (MaterialEditText)findViewById(R.id.editText_confirmPW);
        loginrootLl = (LinearLayout)findViewById(R.id.lLayout_login);

        //绑定事件监听器
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo:确认密码以后执行注册的操作
                if(passwordState==PASSWORD_NOT_CONFIRM) {
                    confirmEt.setVisibility(View.VISIBLE);
                    Toast.makeText(LoginActivity.this, "Please confirm you  password!", Toast.LENGTH_SHORT).show();
                    passwordState = PASSWORD_CONFIRMED;
                }
                else {
                    Toast.makeText(LoginActivity.this, "Registering...", Toast.LENGTH_SHORT).show();
                    ServiceUtil.getInstance().userRegister(LoginActivity.this, userIDEt.getText().toString(), "nickname", passwordEt.getText().toString(), confirmEt.getText().toString());
                }
            }
        });

        logBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo:执行登录操作
                Toast.makeText(LoginActivity.this, "Logging...", Toast.LENGTH_SHORT).show();
                ServiceUtil.getInstance().userLogin(LoginActivity.this, userIDEt.getText().toString(), passwordEt.getText().toString());
                //todo:等待登录结果，生成Internt，传递信息给下一个活动
            }
        });

        tryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo:生成Intent,传递信息给下一个活动
                Intent intent = new Intent(LoginActivity.this, MsgActivity.class);
                startActivity(intent);
            }
        });
    }
}
