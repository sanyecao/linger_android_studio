package com.example.linger.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.linger.R;
import com.example.linger.View.AboutUsCard;
import com.example.linger.View.CommonSettingCard;
import com.example.linger.View.AccountSettingCard;
import com.gc.materialdesign.views.ButtonRectangle;

import it.gmariotti.cardslib.library.view.CardViewNative;

/**
 * Created by Ivan on 2/20/2015.
 */
public class SettingFragment extends Fragment {

    ButtonRectangle logoutBtn;
    MaterialDialog logoutMd;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        return inflater.inflate(R.layout.demo_msg_fragment_seetings, container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        CommonSettingCard card = new CommonSettingCard(getActivity());
        card.init();

        CardViewNative cardViewNative = (CardViewNative)getActivity().findViewById(R.id.cardViewNative_commonSetting);
        cardViewNative.setCard(card);

        AccountSettingCard card1 = new AccountSettingCard(getActivity());
        card1.init();

        CardViewNative cardViewNative1 = (CardViewNative)getActivity().findViewById(R.id.cardViewNative_accountSetting);
        cardViewNative1.setCard(card1);

        AboutUsCard card2 = new AboutUsCard(getActivity());
        card2.init();

        CardViewNative cardViewNative2 = (CardViewNative)getActivity().findViewById(R.id.cardViewNative_aboutUs);
        cardViewNative2.setCard(card2);

        logoutBtn = (ButtonRectangle)getActivity().findViewById(R.id.button_logout);

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getActivity())
                        .title("Log out")
                        .content("You will logout now")
                        .positiveText("Confirm")
                        .negativeText("Cancel")
                        .neutralColorRes(R.color.PrimaryText)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                Toast.makeText(getActivity(), "OK!", Toast.LENGTH_SHORT).show();
                            }
                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();

            }
        });
    }
}
