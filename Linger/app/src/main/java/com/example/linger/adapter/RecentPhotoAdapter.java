package com.example.linger.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

/**
 * Created by Ivan on 2/18/2015.
 */
public class RecentPhotoAdapter extends BaseAdapter {

    private Context context;
    private int itemCount = 8;
    private Uri[] photoLocation = new Uri[8];

    public  RecentPhotoAdapter(Context c) {
        this.context = c;
    }

    @Override
    public int getCount() {
        return photoLocation.length;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
