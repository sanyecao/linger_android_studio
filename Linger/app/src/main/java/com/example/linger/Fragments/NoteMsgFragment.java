package com.example.linger.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.linger.CommentActivity;
import com.example.linger.EditActivity;
import com.example.linger.R;
import com.example.linger.TakePhotoActivity;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.cards.actions.BaseSupplementalAction;
import it.gmariotti.cardslib.library.cards.actions.TextSupplementalAction;
import it.gmariotti.cardslib.library.cards.material.MaterialLargeImageCard;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;

/**
 * Created by Ivan on 3/9/2015.
 */
public class NoteMsgFragment extends Fragment {

    //关于Card的库，参考https://github.com/gabrielemariotti/cardslib
    private CardArrayRecyclerViewAdapter mCardArrayAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        return inflater.inflate(R.layout.msg_fragment_msgview, container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //setup fab menu
        FloatingActionButton addGraffito = (FloatingActionButton)getActivity().findViewById(R.id.floatingMenuItem_addGraffito);
        addGraffito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo:开始编辑涂鸦信息流程，完成后会返回这里
                Toast.makeText(getActivity(), "Add Graffito...", Toast.LENGTH_SHORT).show();
                //以下代码仅为测试使用,去往信息编辑界面
                Intent intent = new Intent(getActivity(), EditActivity.class);
                startActivity(intent);
            }
        });
        final FloatingActionButton addMemo = (FloatingActionButton)getActivity().findViewById(R.id.floatingMenuItem_addMemo);
        addMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo:切换fragment并执行刷新
                Toast.makeText(getActivity(), "Add Memo...", Toast.LENGTH_SHORT).show();
                //以下代码仅为测试使用，去往拍照界面
                Intent intent = new Intent(getActivity(), TakePhotoActivity.class);
                startActivity(intent);
            }
        });
        final FloatingActionButton refreash = (FloatingActionButton)getActivity().findViewById(R.id.floatingMenuItem_refresh);
        refreash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo:根据当前fragment执行刷新操作
                //next:实现下拉刷新
                Toast.makeText(getActivity(), "Refreash...", Toast.LENGTH_SHORT).show();
            }
        });

        //Set the arrayAdapter
        ArrayList<Card> cards = new ArrayList<Card>();
        mCardArrayAdapter = new CardArrayRecyclerViewAdapter(getActivity(), cards);

        CardRecyclerView mRecyclerView = (CardRecyclerView) getActivity().findViewById(R.id.cardRecyclerView_Msg);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Set the empty view
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(mCardArrayAdapter);
        }

        //Load cards
        //todo:使用AsyncTask对Adapter进行操作
        new LoaderAsyncTask().execute();
    }

    class LoaderAsyncTask extends AsyncTask<Void, Void, ArrayList<Card>> {

        LoaderAsyncTask() {
        }

        @Override
        protected ArrayList<Card> doInBackground(Void... params) {
            //elaborate images
            SystemClock.sleep(1000); //delay to simulate download, don't use it in a real app
            if (isAdded()) {
                ArrayList<Card> cards = initCard();
                return cards;
            } else
                return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Card> cards) {
            //Update the adapter
            updateAdapter(cards);
            //displayList();
        }
    }

    private ArrayList<Card> initCard() {

        //Init an array of Cards
        ArrayList<Card> cards = new ArrayList<Card>();
        for (int i = 0; i < 20; i++) {
            ArrayList<BaseSupplementalAction> actions = new ArrayList<BaseSupplementalAction>();

            // 配置卡下面的功能按钮区
            TextSupplementalAction t1 = new TextSupplementalAction(getActivity(), R.id.textView_likeButton);
            t1.setOnActionClickListener(new BaseSupplementalAction.OnActionClickListener() {
                @Override
                public void onClick(Card card, View view) {
                    Toast.makeText(getActivity(), " Click on Text Like ", Toast.LENGTH_SHORT).show();
                    //todo:更新Like数并设置Like的图标为红色
                }
            });
            actions.add(t1);

            TextSupplementalAction t2 = new TextSupplementalAction(getActivity(), R.id.textView_commentButton);
            t2.setOnActionClickListener(new BaseSupplementalAction.OnActionClickListener() {
                @Override
                public void onClick(Card card, View view) {
                    Toast.makeText(getActivity(), " Click on Text COMMENT ", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), CommentActivity.class);
                    //int[] startingLocation = new int[2];
                    //commentBtn.getLocationOnScreen(startingLocation);
                    intent.putExtra(CommentActivity.ARG_DRAWING_START_LOCATION, 400);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0,0);
                }
            });
            actions.add(t2);

            //配置卡片上部的内容
            MaterialLargeImageCard card =
                    MaterialLargeImageCard.with(getActivity())
                            .setTextOverImage("Italian Beaches")
                            .setTitle("This is my favorite local beach")
                            .setSubTitle("A wonderful place")
                            .useDrawableId(R.drawable.sea)
                            .setupSupplementalActions(R.layout.card_supplemental_action, actions)
                            .build();

            //卡片点击事件暂时不需要
//            card.setOnClickListener(new Card.OnCardClickListener() {
//                @Override
//                public void onClick(Card card, View view) {
//                    Toast.makeText(getActivity()," Click on ActionArea ",Toast.LENGTH_SHORT).show();
//                }
//            });
            cards.add(card);
        }

        return cards;
    }
    private void updateAdapter(ArrayList<Card> cards) {
        if (cards != null) {
            mCardArrayAdapter.addAll(cards);
        }
    }
}