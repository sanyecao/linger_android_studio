package com.example.linger;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.WindowManager;

import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;


public class MainActivity extends Activity {

    private final int SPLASH_DISPLAY_TIME=4000;

    ProgressBarCircularIndeterminate processingPb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        processingPb = (ProgressBarCircularIndeterminate)findViewById(R.id.main_processBar);

        //取消标题栏和任务栏，展示全屏效果
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //TODO:加入需要启动的服务等等，现在是简单地延时4秒
        //TODO:检查是否已经登录太确定是否进入登录界面
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                MainActivity.this.startActivity(intent);
                MainActivity.this.finish();
            }
        }, SPLASH_DISPLAY_TIME);
    }

}
