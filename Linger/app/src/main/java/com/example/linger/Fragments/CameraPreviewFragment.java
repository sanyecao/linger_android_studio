package com.example.linger.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.CameraView;
import com.example.linger.R;

/**
 * Created by Ivan on 3/2/2015.
 */
public class CameraPreviewFragment extends CameraFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View content = inflater.inflate(R.layout.takephoto_fragment_camera, container, false);
        CameraView cameraView = (CameraView)content.findViewById(R.id.demo_camera);
        setCameraView(cameraView);
        return content;
    }
}
