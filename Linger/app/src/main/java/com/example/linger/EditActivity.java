package com.example.linger;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.linger.util.RoundedTransformation;
import com.squareup.picasso.Picasso;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardViewNative;

/**
 * Created by Ivan on 2/18/2015.
 */
public class EditActivity extends Activity {
    private final static int SELETE_PIC = 1;
    private final static int CAPTURE_PHOTO = 2;

    private final static int MSG_GRAFFITO = 10;
    private final static int MSG_MEMO = 11;


    private int msgType;

    Uri seletedImage;

    ImageView avatarIv;
    ImageView msgTypeIv;
    ImageView sendIv;
    ImageView addImageIv;
    ImageView addPhotoIv;
    ImageView imageThumbnailTv;

    TextView userNameTv;
    TextView locationInfoTv;
    TextView msgTypeTv;

    EditText titleEt;
    EditText contentEt;

    LinearLayout msgTypeLl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        init();
    }

    private void init() {
        avatarIv = (ImageView)findViewById(R.id.imageView_userAvator);
        msgTypeIv = (ImageView)findViewById(R.id.imageView_messageType);
        sendIv = (ImageView)findViewById(R.id.imageView_send);
        addImageIv = (ImageView)findViewById(R.id.imageView_addImage);
        addPhotoIv = (ImageView)findViewById(R.id.imageView_addPhoto);
        imageThumbnailTv = (ImageView)findViewById(R.id.imageView_imageThumbnail);

        titleEt = (EditText)findViewById(R.id.editText_msgTitle);
        contentEt = (EditText)findViewById(R.id.editText_msgContent);

        userNameTv = (TextView)findViewById(R.id.textView_userName);
        msgTypeTv = (TextView)findViewById(R.id.textView_messageType);
        locationInfoTv = (TextView)findViewById(R.id.textView_locationInfo);

        msgTypeLl = (LinearLayout)findViewById(R.id.linearLayout_msgType);

        msgType = MSG_GRAFFITO;

        Picasso.with(this)
                .load(R.drawable.default_photo)
                .resize(56,56)
                .centerCrop()
                .transform(new RoundedTransformation())
                .into(avatarIv);
        msgTypeIv.setImageResource(R.drawable.ic_gesture_pc_18dp);
        userNameTv.setText(R.string.default_id);
        msgTypeTv.setText(R.string.Graffito);
        locationInfoTv.setText(R.string.default_location);

        //todo:绑定监听器

        addImageIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 1);
            }
        });

        msgTypeLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(msgType == MSG_GRAFFITO) {
                    msgType = MSG_MEMO;
                    msgTypeTv.setText("Memo");
                    msgTypeIv.setImageResource(R.drawable.ic_create_pc_18dp);
                    Toast.makeText(getBaseContext(), "Memo need a key when published!", Toast.LENGTH_SHORT).show();
                }
                else {
                    msgType = MSG_GRAFFITO;
                    msgTypeTv.setText("Graffito");
                    msgTypeIv.setImageResource(R.drawable.ic_gesture_pc_18dp);
                    Toast.makeText(getBaseContext(), "Fell free to add a graffito!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK) {
            try {
                seletedImage = data.getData();
                Picasso.with(this)
                        .load(seletedImage)
                        .resize(72,72)
                        .into(imageThumbnailTv);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
