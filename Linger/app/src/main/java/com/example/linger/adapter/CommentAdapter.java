package com.example.linger.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.linger.R;
import com.example.linger.util.RoundedTransformation;
import com.squareup.picasso.Picasso;

/**
 * Created by Ivan on 2/16/2015.
 */
public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;

    private int photoSize;
    private int itemsCount = 0;

    private int lastAnimationPosition = -1;

    private boolean animationLocked = false;
    private boolean delayEnterAnimation = true;

    public CommentAdapter(Context context) {
        this.context = context;
        photoSize = context.getResources().getDimensionPixelSize(R.dimen.comment_photo_size);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //创建Holder
        final View view = LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        runEnterAnimation(viewHolder.itemView, position);
        CommentViewHolder holder = (CommentViewHolder) viewHolder;
        switch (position % 3) {
            case 0:
                holder.comment.setText("Lorem ipsum dolor sit amet, consectetur adipisicing elit.");
                break;
            case 1:
                holder.comment.setText("Cupcake ipsum dolor sit amet bear claw.");
                break;
            case 2:
                holder.comment.setText("Cupcake ipsum dolor sit. Amet gingerbread cupcake. Gummies ice cream dessert icing marzipan apple pie dessert sugar plum.");
                break;
        }
        Picasso.with(context)
                .load(R.drawable.default_photo)
                .centerCrop()
                .resize(photoSize, photoSize)
                .transform(new RoundedTransformation())
                .into(holder.comment_photo);
    }

    @Override
    public int getItemCount() {
        return itemsCount;
    }

    public void updateItems() {
        itemsCount = 10;
        notifyDataSetChanged();
    }

    public void addItem() {
        itemsCount++;
        notifyItemInserted(itemsCount-1);
    }
    public static class CommentViewHolder extends RecyclerView.ViewHolder {

        ImageView comment_photo;
        TextView comment;
        public CommentViewHolder(View view) {
            super(view);
            comment = (TextView)view.findViewById(R.id.comment);
            comment_photo = (ImageView)view.findViewById(R.id.comment_photo);
        }
    }

    private void runEnterAnimation(View view, int position) {
        if(animationLocked) {
            return;
        }

        if (position > lastAnimationPosition) {
            lastAnimationPosition = position;
            //设置Y方向的位移
            view.setTranslationY(100);
            //设置透明度，10以下就很透明了~
            view.setAlpha(0.f);
            view.animate()
                    .translationY(0).alpha(0.f)
                    .setStartDelay(delayEnterAnimation?20*position:0)
                    .setInterpolator(new DecelerateInterpolator(2.f))
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            animationLocked = true;
                        }
                    })
                    .start();

        }
    }
    public void setAnimationLocked(boolean lockAnimation) {
        this.animationLocked = lockAnimation;
    }

    public void setDelayEnterAnimation(boolean delayEnterAnimation) {
        this.delayEnterAnimation = delayEnterAnimation;
    }
}
