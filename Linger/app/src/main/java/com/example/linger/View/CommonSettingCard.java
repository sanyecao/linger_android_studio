package com.example.linger.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.linger.R;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;

/**
 * Created by Ivan on 2/26/2015.
 */
public class CommonSettingCard extends CardWithList {

    private CommonSettingObject s1;
    private CommonSettingObject s2;
    private CommonSettingObject s3;
    private CommonSettingObject s4;

    public CommonSettingCard(Context context) {
        super(context);
    }

    public void setSelectImageListener(View.OnClickListener onClickListener) {
        s3.onClickListener = onClickListener;
        s4.onClickListener = onClickListener;
    }

    @Override
    public void init() {
        s1 = new CommonSettingObject(this);
        s2 = new CommonSettingObject(this);
        s3 = new CommonSettingObject(this);
        s4 = new CommonSettingObject(this);
        super.init();
    }

    @Override
    protected CardHeader initCardHeader() {
        CardHeader cardHeader = new CardHeader(getContext());
        return cardHeader;
    }

    @Override
    protected void initCard() {
        setUseEmptyView(false);
    }
    @Override
    protected List<ListObject> initChildren() {

        List<ListObject> mObjects = new ArrayList<ListObject>();


        s1.title = "User Name";
        s1.description = "Your ID in Linger";
        s1.onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Change id", Toast.LENGTH_SHORT).show();
            }
        };
        mObjects.add(s1);

        s2.title = "Gender";
        s2.description = "Male or Female?";
        s2.onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .title("Your Gender")
                        .items(R.array.gender)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int i, CharSequence charSequence) {

                            }
                        })
                        .positiveText("Confirm")
                        .positiveColorRes(R.color.AccentColor)
                        .show();
            }
        };

        mObjects.add(s2);

        s3.title = "Avator";
        s3.description = "Setting your avator";
        s3.onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Change avator is still in testing!", Toast.LENGTH_SHORT).show();
                
            }
        };
        mObjects.add(s3);

        s4.title = "Account Background";
        s4.description = "Custom background";
        s4.onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Change background is still in testing!", Toast.LENGTH_SHORT).show();
            }
        };
        mObjects.add(s4);
        return mObjects;
    }

    @Override
    public int getChildLayoutId() {
        return R.layout.common_setting_inner_main;
    }

    @Override
    public View setupChildView(int childPosition, ListObject object, View convertView, ViewGroup parent){
        TextView itemTitle = (TextView)convertView.findViewById(R.id.textView_commonItemTitle);
        TextView itemDescription = (TextView)convertView.findViewById(R.id.textView_commonItemDescription);
        ImageButton changeSetting = (ImageButton)convertView.findViewById(R.id.imageButton_changeCommonSetting);

        CommonSettingObject settingObject = (CommonSettingObject)object;
        itemTitle.setText(settingObject.title);
        itemDescription.setText(settingObject.description);

        changeSetting.setOnClickListener(settingObject.onClickListener);

        return convertView;
    }

    public class CommonSettingObject extends DefaultListObject {

        public String title;
        public String description;
        public View.OnClickListener onClickListener;

        public CommonSettingObject(Card parentCard) {
            super(parentCard);
        }
    }
}
