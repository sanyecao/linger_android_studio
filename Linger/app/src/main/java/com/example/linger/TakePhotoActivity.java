package com.example.linger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.CameraHostProvider;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;
import com.example.linger.Fragments.CameraPreviewFragment;

/**
 * Created by Ivan on 3/1/2015.
 */
public class TakePhotoActivity extends Activity implements CameraHostProvider {

    public static final int FOR_HASH = 1;
    public static final int FOR_PICTURE = 2;
    public static final int HASH_OK = 1;
    public static final int PICTURE_OK = 2;

    private static final int CAPTURE_PHOTO = 0;
    private static final int PREVIEW_PHOTO = 1;

    private CameraPreviewFragment current = null;

    private int current_state = CAPTURE_PHOTO;
    private int purpose;


    Button takePhotoBtn;
    ImageView previewIv;
    ImageButton cancelPhotoIb;
    ImageButton confirmPhotoIb;

    Bitmap capturedPhoto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        current = new CameraPreviewFragment();

        setContentView(R.layout.activity_takephoto);
        getFragmentManager().beginTransaction().replace(R.id.frameLayout_cameraContainer, current).commit();
        confirmPhotoIb = (ImageButton)findViewById(R.id.imageButton_confirmPhoto);
        cancelPhotoIb = (ImageButton)findViewById(R.id.imageButton_cancelPhoto);
        takePhotoBtn = (Button)findViewById(R.id.demo_takePhotoButton);

        final Intent intent = getIntent();
        purpose = intent.getIntExtra("PURPOSE", FOR_HASH);

        takePhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(current_state == CAPTURE_PHOTO) {
                    current_state = PREVIEW_PHOTO;
                    takePhotoBtn.setEnabled(false);
                    current.takePicture(true, false);
                    confirmPhotoIb.setVisibility(View.VISIBLE);
                }
            }
        });


        confirmPhotoIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(purpose == FOR_HASH) {
                    //TODO:返回HASH数据
                    //Bundle bundle = new Bundle();

                }
                else if (purpose == FOR_PICTURE) {
                    //TODO:返回BITMAP
                }
            }
        });

        cancelPhotoIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        previewIv = (ImageView)findViewById(R.id.demo_previewPhoto);
    }

    @Override
    public void onBackPressed() {
        if(current_state == CAPTURE_PHOTO){
            super.onBackPressed();
        }
        else if(current_state == PREVIEW_PHOTO){
            previewIv.setVisibility(View.GONE);
            current.restartPreview();
            takePhotoBtn.setEnabled(true);
            current_state = CAPTURE_PHOTO;
        }
    }

    @Override
    public CameraHost getCameraHost() {
        return (new DemoCameraHost(this));
    }

    private class DemoCameraHost extends SimpleCameraHost {

        private Camera.Size previewSize;

        public DemoCameraHost (Context context) {
            super(context);
        }

        @Override
        public void saveImage(PictureTransaction xact, final Bitmap bitmap) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showTakenPicture(bitmap);
                }
            });
        }

        @Override
        public boolean useSingleShotMode() {
            return true;
        }
    }

    private void showTakenPicture(Bitmap bitmap) {
        capturedPhoto = bitmap;
        previewIv.setImageBitmap(bitmap);
        previewIv.setVisibility(View.VISIBLE);
    }
}
