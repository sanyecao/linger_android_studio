package com.example.linger.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;

public class ServiceUtil
{
    private static ServiceUtil serviceUtil = null;

    private ServiceUtil()
    {
        return;
    }

    public static  ServiceUtil getInstance()
    {
        if(serviceUtil == null)
            serviceUtil = new  ServiceUtil();
        return serviceUtil;
    }

    private class UserRegisterTask extends AsyncTask<String, Void, String>
    {
        private Context context;
        public UserRegisterTask(Context context)
        {
            this.context = context;
        }

        private boolean hasError;
        @Override
        protected String doInBackground(String... params)
        {
            String response = null;
            hasError = false;
            JSONObject jsonform=new JSONObject();
            JSONObject json=new JSONObject();
            try
            {
                jsonform.put("email", params[0]);
                jsonform.put("nickname", params[1]);
                jsonform.put("password", params[2]);
                jsonform.put("password_confirmation", params[3]);
                json.put("user", jsonform);
            }
            catch (JSONException e)
            {
                hasError = true;
                return "Something Wrong with Register Json!";
            }
            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpClientParams.setCookiePolicy(httpClient.getParams(), CookiePolicy.BROWSER_COMPATIBILITY);
                HttpPost httpPost = new HttpPost("http://107.170.248.219:3000/users");
                StringEntity se = new StringEntity(json.toString(),"utf-8");
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Accept", "application/json");
                httpPost.setEntity(se);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                if(httpResponse == null)
                {
                    hasError = true;
                    return "Register Not Succeed : response is null";
                }
                response = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                if(httpResponse.getStatusLine().getStatusCode() != 200)
                {
                    hasError = true;
                    return "Register Not Succeed(Error 200) : " + response;
                }
            }
            catch (Exception e)
            {
                hasError = true;
                return "Something Wrong with Register HttpGet!";
            }
            return response;
        }

        @Override
        protected void onPostExecute(final String result)
        {
            if(hasError)
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void userRegister(final Context context, final String... params)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                UserRegisterTask userRegisterTask = new UserRegisterTask(context);
                userRegisterTask.execute(params);
            }
        }).start();
    }

    private class UserLoginTask extends AsyncTask<String, Void, String>
    {
        private Context context;
        public UserLoginTask(Context context)
        {
            this.context = context;
        }

        private boolean hasError;
        @Override
        protected String doInBackground(String... params)
        {
            String response = null;
            hasError = false;
            JSONObject jsonform=new JSONObject();
            JSONObject json=new JSONObject();
            try
            {
                jsonform.put("email", params[0]);
                jsonform.put("password", params[1]);
                json.put("session", jsonform);
            }
            catch (JSONException e)
            {
                hasError = true;
                return "Something Wrong with Login Json!";
            }
            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpClientParams.setCookiePolicy(httpClient.getParams(), CookiePolicy.BROWSER_COMPATIBILITY);
                HttpPost httpPost = new HttpPost("http://107.170.248.219:3000/sessions");
                StringEntity se = new StringEntity(json.toString(),"utf-8");
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Accept", "application/json");
                httpPost.setEntity(se);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                if(httpResponse == null)
                {
                    hasError = true;
                    return "Login Not Succeed : response is null";
                }
                response = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                if(httpResponse.getStatusLine().getStatusCode() != 200)
                {
                    hasError = true;
                    return "Login Not Succeed(Error 200) : " + response;
                }
                if(((DefaultHttpClient)httpClient).getCookieStore().getCookies().isEmpty())
                {
                    hasError = true;
                    return "Cookie is empty!";
                }
                else
                {
                    Global.cookie = ((DefaultHttpClient)httpClient).getCookieStore();
                    return response;
                }
            }
            catch (Exception e)
            {
                hasError = true;
                return "Something Wrong with Login HttpGet!";
            }
            //return response;
        }

        @Override
        protected void onPostExecute(final String result)
        {
            if(hasError)
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void userLogin(final Context context, final String... params)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                UserLoginTask userLoginTask = new UserLoginTask(context);
                userLoginTask.execute(params);
            }
        }).start();
    }

    private class GeoGetTask extends AsyncTask<Void, Void, String>
    {
        private Context context;
        public GeoGetTask(Context context)
        {
            this.context = context;
        }

        private boolean hasError;

        @Override
        protected String doInBackground(Void... params)
        {
            hasError  = false;
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            List<String> providerList = locationManager.getProviders(true);
            String provider;
            if (providerList.contains(LocationManager.GPS_PROVIDER))
            {
                provider = LocationManager.GPS_PROVIDER;
            }
            else if (providerList.contains(LocationManager.NETWORK_PROVIDER))
            {
                provider = LocationManager.NETWORK_PROVIDER;
            }
            else
            {
                hasError = true;
                return "No Location Provider To Use!";
            }

            Location location = locationManager.getLastKnownLocation(provider);
            if(location == null)
            {
                hasError = true;
                return "No Location Get!";
            }

            String response = null;
            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpClientParams.setCookiePolicy(httpClient.getParams(), CookiePolicy.BROWSER_COMPATIBILITY);
                HttpGet httpGet = new HttpGet("http://api.map.baidu.com/geocoder/v2/?ak=uFADD7Rq6o4LEvHvgtcg0dxs&callback=renderReverse&mcode=F7:31:59:EE:58:0A:65:E6:48:CD:2D:BE:72:66:1A:60:73:0E:1B:3E;com.example.linger&location="+location.getLatitude()+","+location.getLongitude()+"&output=json&pois=0");
                HttpResponse httpResponse = httpClient.execute(httpGet);
                if(httpResponse == null)
                {
                    hasError = true;
                    return "Baidu Map Not Succeed : response is null";
                }
                response = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                if(httpResponse.getStatusLine().getStatusCode() != 200)
                {
                    hasError = true;
                    return "Baidu Map Not Succeed(Error 200) : " + response;
                }
            }
            catch (Exception e)
            {
                hasError = true;
                return "Something Wrong with Geo HttpGet!";
            }

            String address = null;
            try
            {
                response = response.replace("renderReverse&&renderReverse","");
                response = response.replace("(","");
                response = response.replace(")","");
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if(status != 0)
                {
                    hasError = true;
                    return "Baidu Map Bad Return Status : " + response;
                }
                address = jsonObject.getJSONObject("result").getString("formatted_address");
            }
            catch(JSONException e)
            {
                hasError = true;
                Toast.makeText(context, "Something Wrong with Map Json!", Toast.LENGTH_SHORT).show();
            }

            return address;
        }

        @Override
        protected void onPostExecute(String result)
        {
            if(hasError)
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
                Global.LastLocation = result;
            }
        }

    }

    public void geoGet(final Context context)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                GeoGetTask geoGetTask = new GeoGetTask(context);
                geoGetTask.execute();
            }
        }).start();
    }

    private class GraffitoGetTask extends AsyncTask<Void, Void, String>
    {
        private Context context;
        public GraffitoGetTask(Context context)
        {
            this.context = context;
        }

        private boolean hasError;

        @Override
        protected String doInBackground(Void... params)
        {
            hasError  = false;
            String response = null;
            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpClientParams.setCookiePolicy(httpClient.getParams(), CookiePolicy.BROWSER_COMPATIBILITY);
                HttpGet httpGet = new HttpGet("http://107.170.248.219:3000/public_messages.json");
                if(Global.cookie != null)
                    ((DefaultHttpClient)httpClient).setCookieStore(Global.cookie);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                if(httpResponse == null)
                {
                    hasError = true;
                    return "Graffito Get Not Succeed : Response is null";
                }
                response = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                if(httpResponse.getStatusLine().getStatusCode() != 200)
                {
                    hasError = true;
                    return "Graffito Get Not Succeed(Error 200) : " + response;
                }

                return response;
            }
            catch(Exception e)
            {
                hasError = true;
                return "Something Wrong with Graffito HttpGet!";
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            if(hasError)
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void graffitoGet(final Context context)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                GraffitoGetTask graffitoGetTask = new GraffitoGetTask(context);
                graffitoGetTask.execute();
            }
        }).start();
    }

    private class GraffitoPostTask extends AsyncTask<String, Void, String>
    {
        private Context context;
        public GraffitoPostTask(Context context)
        {
            this.context = context;
        }

        private boolean hasError;

        @Override
        protected String doInBackground(String... params)
        {
            hasError  = false;
            String response = null;

            JSONObject jsonform=new JSONObject();
            JSONObject json=new JSONObject();
            try
            {
                jsonform.put("content", params[0]);
                jsonform.put("geolocation", params[1]);
                json.put("public_message", jsonform);
            }
            catch (JSONException e)
            {
                hasError = true;
                return "Something Wrong with GraffitoPost Json!";
            }

            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpClientParams.setCookiePolicy(httpClient.getParams(), CookiePolicy.BROWSER_COMPATIBILITY);
                HttpPost httpPost = new HttpPost("http://107.170.248.219:3000/public_messages.json");
                StringEntity se = new StringEntity(json.toString(),"utf-8");
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Accept", "application/json");
                httpPost.setEntity(se);
                if(Global.cookie != null)
                    ((DefaultHttpClient)httpClient).setCookieStore(Global.cookie);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                if(httpResponse == null)
                {
                    hasError = true;
                    return "Graffito Get Not Succeed : response is null";
                }
                response = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                if(httpResponse.getStatusLine().getStatusCode() != 200)
                {
                    hasError = true;
                    return "Graffito Post Not Succeed(Error 200) : " + response;
                }
                return response;
            }
            catch(Exception e)
            {
                hasError = true;
                return "Something Wrong with Graffito HttpPost!";
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            if(hasError)
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void graffitoPost(final Context context, final String... params)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                GraffitoPostTask graffitoPostTask = new GraffitoPostTask(context);
                graffitoPostTask.execute(params);
            }
        }).start();
    }


    private class ImagePostTask extends AsyncTask<String, Void, String>
    {
        private Context context;
        public ImagePostTask(Context context)
        {
            this.context = context;
        }

        private boolean hasError;

        @Override
        protected String doInBackground(String... params)
        {
            hasError  = false;
            String response = null;

            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpClientParams.setCookiePolicy(httpClient.getParams(), CookiePolicy.BROWSER_COMPATIBILITY);
                HttpPost httpPost = new HttpPost("http://107.170.248.219:3000/pictures/upload");
                HttpEntity httpEntity = MultipartEntityBuilder.create().
                        setMode(HttpMultipartMode.BROWSER_COMPATIBLE).
                        //setCharset(Charset.forName(HTTP.UTF_8)).
                        addPart("pictures[file]", new FileBody(new File(params[0]), "image/jpeg")).
                        build();
                //MultipartEntity httpEntity = new MultipartEntity();
                httpPost.addHeader(HTTP.CONTENT_TYPE, ContentType.MULTIPART_FORM_DATA.toString() + "charset =" + HTTP.UTF_8);
                //httpEntity.addPart("pictures[file]", new FileBody(new File(params[0]), "image/jpeg"));
                //httpPost.addHeader(HTTP.CHARSET_PARAM,HTTP.UTF_8);
                //httpPost.addHeader("action","/pictures/upload");
                httpPost.setEntity(httpEntity);
                if(Global.cookie != null)
                    ((DefaultHttpClient)httpClient).setCookieStore(Global.cookie);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                if(httpResponse == null)
                {
                    hasError = true;
                    return "Image Post Not Succeed : response is null";
                }
                response = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                if(httpResponse.getStatusLine().getStatusCode() != 200)
                {
                    hasError = true;
                    return "Image Post Not Succeed(Error 200) : " + response;
                }
                return response;
            }
            catch(Exception e)
            {
                hasError = true;
                return "Something Wrong with Image HttpPost!";
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            if(hasError)
            {
                Toast.makeText(context, result, Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(context, result, Toast.LENGTH_LONG).show();
            }
        }

    }

    public void imagePost(final Context context, final String... params)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                ImagePostTask imagePostTask = new ImagePostTask(context);
                imagePostTask.execute(params);
            }
        }).start();
    }
}
