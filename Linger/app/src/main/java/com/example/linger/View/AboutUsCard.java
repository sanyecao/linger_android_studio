package com.example.linger.View;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.linger.R;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;
import it.gmariotti.cardslib.library.prototypes.LinearListView;

/**
 * Created by Ivan on 2/28/2015.
 */
public class AboutUsCard extends CardWithList {

    public AboutUsCard(Context context) {
        super(context);
    }

    @Override
    protected CardHeader initCardHeader() {
        CardHeader cardHeader = new CardHeader(getContext());
        return cardHeader;
    }

    @Override
    protected void initCard() {
        setUseEmptyView(false);
    }
    @Override
    protected List<ListObject> initChildren() {

        List<ListObject> mObjects = new ArrayList<ListObject>();

        AboutUsObject s1 = new AboutUsObject(this);
        s1.title = "Feedback";
        s1.icon = R.drawable.ic_email_black_18dp;
        s1.setObjectId(s1.title);
        s1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(LinearListView linearListView, View view, int i, ListObject object) {
                new MaterialDialog.Builder(getContext())
                        .content("Please contact 11300720181@fudan.edu.cn to feedback!")
                        .positiveText("Confirm")
                        .positiveColorRes(R.color.AccentColor)
                        .show();
            }
        });
        mObjects.add(s1);

        AboutUsObject s2 = new AboutUsObject(this);
        s2.title = "About Us";
        s2.icon = R.drawable.ic_info_black_18dp;
        s2.setObjectId(s2.title);
        s2.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(LinearListView linearListView, View view, int i, ListObject object) {
                new MaterialDialog.Builder(getContext())
                        .content("Linger Team present")
                        .positiveText("Confirm")
                        .positiveColorRes(R.color.AccentColor)
                        .show();
            }
        });
        mObjects.add(s2);

        return mObjects;
    }

    @Override
    public int getChildLayoutId() {
        return R.layout.aboutus_setting_inner_main;
    }

    @Override
    public View setupChildView(int childPosition, ListObject object, View convertView, ViewGroup parent){
        TextView itemTitle = (TextView)convertView.findViewById(R.id.textView_aboutUsTitle);
        ImageView icon = (ImageView)convertView.findViewById(R.id.imageView_aboutUs);

        AboutUsObject settingObject = (AboutUsObject)object;
        itemTitle.setText(settingObject.title);
        icon.setImageResource(settingObject.icon);

        return convertView;
    }

    public class AboutUsObject extends DefaultListObject {

        public String title;
        public int icon;

        public AboutUsObject(Card parentCard) {

            super(parentCard);
            initAboutUsObj();
        }

        private void initAboutUsObj() {
            setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(LinearListView linearListView, View view, int i, ListObject object) {
                    Toast.makeText(getContext(), "Click on " + getId(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
