package com.example.linger.View;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.linger.R;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;
import it.gmariotti.cardslib.library.prototypes.LinearListView;

/**
 * Created by Ivan on 2/26/2015.
 */
public class AccountSettingCard extends CardWithList {

    public AccountSettingCard(Context context) {
        super(context);
    }

    @Override
    protected CardHeader initCardHeader() {
        CardHeader cardHeader = new CardHeader(getContext());
        return cardHeader;
    }

    @Override
    protected void initCard() {
        setUseEmptyView(false);
    }
    @Override
    protected List<ListObject> initChildren() {

        List<ListObject> mObjects = new ArrayList<ListObject>();

        AccountSettingObject s1 = new AccountSettingObject(this);
        s1.title = "Password";
        s1.description = "Change your password";
        s1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(LinearListView linearListView, View view, int i, ListObject object) {
                new MaterialDialog.Builder(getContext())
                        .title("Change Password")
                        .customView(R.layout.dialog_setting_password_layout, true)
                        .positiveText("Confirm")
                        .positiveColorRes(R.color.AccentColor)
                        .negativeText("Cancel")
                        .negativeColor(R.color.PrimaryText)
                        .show();
                Toast.makeText(getContext(), "Change password", Toast.LENGTH_SHORT).show();
            }
        });
        mObjects.add(s1);

        AccountSettingObject s2 = new AccountSettingObject(this);
        s2.title = "Mobile Phone";
        s2.description = "Bind your Mobile Phone";
        s2.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(LinearListView linearListView, View view, int i, ListObject object) {
                Toast.makeText(getContext(), "Sorry, but this function is not available now!", Toast.LENGTH_SHORT).show();
            }
        });
        mObjects.add(s2);

        return mObjects;
    }

    @Override
    public int getChildLayoutId() {
        return R.layout.account_setting_inner_main;
    }

    @Override
    public View setupChildView(int childPosition, ListObject object, View convertView, ViewGroup parent){
        TextView itemTitle = (TextView)convertView.findViewById(R.id.textView_accountItemTitle);
        TextView itemDescription = (TextView)convertView.findViewById(R.id.textView_accountItemDescription);
        ImageButton changeSetting = (ImageButton)convertView.findViewById(R.id.imageButton_changeAccountSetting);

        AccountSettingObject settingObject = (AccountSettingObject)object;
        itemTitle.setText(settingObject.title);
        itemDescription.setText(settingObject.description);

        return convertView;
    }

    public class AccountSettingObject extends DefaultListObject {

        public String title;
        public String description;

        public AccountSettingObject(Card parentCard) {
            super(parentCard);
        }
    }
}
